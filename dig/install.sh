#!/bin/bash

source functions.sh

print_blue "Installing Dig..."

MAJORVERSION=9
PKGVERSION=9.18.1

requirements(){
  print_green "\t[REQUIREMENTS] Getting requirements..."
  apt-get -qq install -y gpg libuv1-dev libnghttp2-dev libcap-dev libxml2-dev libjson-c-dev > /dev/null
}

download(){
  print_green "\t[DOWNLOAD] Getting source code..."
  wget -q "https://ftp.isc.org/isc/bind${MAJORVERSION}/${PKGVERSION}/bind-${PKGVERSION}.tar.xz" -O "bind-${PKGVERSION}.tar.xz"
}

verify(){
  print_green "\t[VERIFY] Verifying package..."
  echo "57c7afd871694d615cb4defb1c1bd6ed023350943d7458414db8d493ef560427 bind-${PKGVERSION}.tar.xz" > "bind-${PKGVERSION}.tar.xz.sha256sum"
  sha256sum -c "bind-${PKGVERSION}.tar.xz.sha256sum" > /dev/null
}

prepare(){
  print_green "\t[PREPARE] Preparing build..."
  mkdir "bind-${PKGVERSION}"
  tar -xf "bind-${PKGVERSION}.tar.xz" -C "bind-${PKGVERSION}" --strip-components 1
  pushd "bind-${PKGVERSION}" > /dev/null
  ./configure > /dev/null
}


build() {
  print_green "\t[BUILD] Building package..."
  make "${MAKEFLAGS}" -C lib/isc    > /dev/null &&
  make "${MAKEFLAGS}" -C lib/dns    > /dev/null&&
  make "${MAKEFLAGS}" -C lib/ns     > /dev/null&&
  make "${MAKEFLAGS}" -C lib/isccfg > /dev/null&&
  make "${MAKEFLAGS}" -C lib/bind9  > /dev/null&&
  make "${MAKEFLAGS}" -C lib/irs    > /dev/null &&
  make "${MAKEFLAGS}" -C bin/dig    > /dev/null
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  make -C lib/isc    install > /dev/null &&
  make -C lib/dns    install > /dev/null &&
  make -C lib/ns     install > /dev/null &&
  make -C lib/isccfg install > /dev/null &&
  make -C lib/bind9  install > /dev/null &&
  make -C lib/irs    install > /dev/null &&
  make -C bin/dig    install > /dev/null
  popd > /dev/null
}

cleanup(){
  print_green "\t[CLEANUP] Cleaning up..."
  rm -rf "bind-${PKGVERSION}"  "bind-${PKGVERSION}.tar.xz" "bind-${PKGVERSION}.tar.xz.sha256sum"
}

pushd sources/ > /dev/null

requirements
download
verify
prepare
build
install_package
cleanup

popd > /dev/null

unset PKGVERSION
unset MAJORVERSION
