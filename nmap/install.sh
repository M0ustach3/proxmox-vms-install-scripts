#!/bin/bash

source functions.sh

print_blue "Installing Nmap..."

MAJORVERSION=7
PKGVERSION=7.92

requirements(){
  apt-get install libpcre3-dev -qq > /dev/null
}

download(){
  print_green "\t[DOWNLOAD] Getting source code..."
  wget -q "https://nmap.org/dist/nmap-${PKGVERSION}.tgz" -O "nmap-${PKGVERSION}.tgz"
}

verify(){
  print_green "\t[VERIFY] Verifying package..."
  echo "064183ea642dc4c12b1ab3b5358ce1cef7d2e7e11ffa2849f16d339f5b717117 nmap-${PKGVERSION}.tgz" > "nmap-${PKGVERSION}.tgz.sha256sum"
  sha256sum -c "nmap-${PKGVERSION}.tgz.sha256sum" > /dev/null
}

prepare(){
  print_green "\t[PREPARE] Preparing build..."
  mkdir "nmap-${PKGVERSION}"
  tar -xf "nmap-${PKGVERSION}.tgz" -C "nmap-${PKGVERSION}" --strip-components 1
  pushd "nmap-${PKGVERSION}" > /dev/null
  patch -Np1 "nmap.h" "../../nmap/patch_nmap.patch" > /dev/null
  ./configure --without-zenmap > /dev/null
}


build() {
  print_green "\t[BUILD] Building package..."
  make "${MAKEFLAGS}" > /dev/null
  return 0
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  make install > /dev/null
  popd > /dev/null
}

cleanup(){
  print_green "\t[CLEANUP] Cleaning up..."
  rm -rf "nmap-${PKGVERSION}.tgz" "nmap-${PKGVERSION}" "nmap-${PKGVERSION}.tgz.sha256sum"
}

pushd sources/ > /dev/null

requirements
download
verify
prepare
build
install_package
cleanup

popd > /dev/null

unset PKGVERSION
unset MAJORVERSION
