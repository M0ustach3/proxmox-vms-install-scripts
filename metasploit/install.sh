source functions.sh

print_blue "Installing metasploit..."

MAJORVERSION=
PKGVERSION=


requirements(){
  apt-get install curl -qq > /dev/null
}

download(){
  print_green "\t[DOWNLOAD] Getting source code..."
  curl -s https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall
  chmod 755 msfinstall

}

verify(){
  return 0
}

prepare(){
  return 0
}


build() {
  return 0
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  patch "msfinstall" "../metasploit/msfinstall.patch" > /dev/null
  ./msfinstall
}

cleanup(){
  rm -rf msfinstall
  return 0
}

pushd sources/ > /dev/null

requirements
download
verify
prepare
build
install_package
cleanup

popd > /dev/null

unset PKGVERSION
unset MAJORVERSION
