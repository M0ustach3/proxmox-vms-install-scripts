RED="\e[31m"
GREEN="\e[32m"
BLUE="\e[34m"
DEFAULT="\e[39m"

print_blue(){
  echo -e "${BLUE} ${1} ${DEFAULT}"
}

print_red(){
  echo -e "${RED} ${1} ${DEFAULT}"
}

print_green(){
  echo -e "${GREEN} ${1} ${DEFAULT}"
}

export -f print_green
export -f print_blue
export -f print_red
