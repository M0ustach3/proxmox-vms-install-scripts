#!/bin/bash

source functions.sh

print_blue "Installing SQLmap"

MAJORVERSION=
PKGVERSION=

requirements(){
  print_green "\t[REQUIREMENTS] Getting requirements..."
  apt-get install python3 -qq > /dev/null
}

download(){
  print_green "\t[DOWNLOAD] Getting source code..."
  wget -q "https://github.com/sqlmapproject/sqlmap/tarball/master" -O "sqlmap-latest.tar.gz"
}

verify(){
  print_green "\t[VERIFY] Verifying package..."
  echo "6270091880a555f64db802f9df8750a38ea92683059fcbb7a78c7319f93bffb7 sqlmap-latest.tar.gz" > "sqlmap-latest.tar.gz.sha256sum"
  sha256sum -c "sqlmap-latest.tar.gz.sha256sum" > /dev/null
}

prepare(){
  print_green "\t[PREPARE] Preparing build..."
  mkdir "sqlmap-latest"
  tar -xf "sqlmap-latest.tar.gz" -C "sqlmap-latest" --strip-components 1
}


build() {
  return 0
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  mkdir -p /opt/sqlmap
  cp -r sqlmap-latest/* /opt/sqlmap
  chmod +x /opt/sqlmap/sqlmap.py
  cat << 'EOF' > /bin/sqlmap
#!/bin/bash
python3 /opt/sqlmap/sqlmap.py "$@"
EOF
  chmod +x /bin/sqlmap
}

cleanup(){
  print_green "\t[CLEANUP] Cleaning up..."
  rm -rf "sqlmap-latest"  "sqlmap-latest.tar.gz" "sqlmap-latest.tar.gz.sha256sum"
}

requirements
download
verify
prepare
build
install_package
cleanup

unset PKGVERSION
unset MAJORVERSION
