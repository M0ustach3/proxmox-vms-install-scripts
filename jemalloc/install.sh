source functions.sh

print_blue "Installing jemalloc..."

MAJORVERSION=5
PKGVERSION=5.2.1


requirements(){
  return 0
}

download(){
  print_green "\t[DOWNLOAD] Getting source code..."
  wget -q "https://github.com/jemalloc/jemalloc/releases/download/${PKGVERSION}/jemalloc-${PKGVERSION}.tar.bz2" -O "jemalloc-${PKGVERSION}.tar.bz2"
}

verify(){
  print_green "\t[VERIFY] Verifying package..."
  echo "34330e5ce276099e2e8950d9335db5a875689a4c6a56751ef3b1d8c537f887f6 jemalloc-${PKGVERSION}.tar.bz2" > "jemalloc-${PKGVERSION}.tar.bz2.sha256sum"
  sha256sum -c "jemalloc-${PKGVERSION}.tar.bz2.sha256sum" > /dev/null
}

prepare(){
  print_green "\t[PREPARE] Preparing build..."
  mkdir "jemalloc-${PKGVERSION}"
  tar -xf "jemalloc-${PKGVERSION}.tar.bz2" -C "jemalloc-${PKGVERSION}" --strip-components 1
  pushd "jemalloc-${PKGVERSION}" > /dev/null
  ./configure > /dev/null
}


build() {
  print_green "\t[BUILD] Building package..."
  make "${MAKEFLAGS}" > /dev/null
  return 0
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  make install > /dev/null
  ldconfig > /dev/null
  popd > /dev/null
}

cleanup(){
  print_green "\t[CLEANUP] Cleaning up..."
  rm -rf "jemalloc-${PKGVERSION}.tar.bz2" "jemalloc-${PKGVERSION}" "jemalloc-${PKGVERSION}.tar.bz2.sha256sum"
}

pushd sources/ > /dev/null

requirements
download
verify
prepare
build
install_package
cleanup

popd > /dev/null

unset PKGVERSION
unset MAJORVERSION
