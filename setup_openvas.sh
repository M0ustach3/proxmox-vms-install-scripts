#!/bin/bash

# _____      _     _         ____                  _ _              _           _   _   _
# |  __ \    | |   | |       |  _ \                | (_)            | |         | | | | (_)
# | |__) |_ _| |__ | | ___   | |_) | ___  _ __   __| |_  __ _ ______| |    _   _| |_| |_ _  __ _ _   _
# |  ___/ _` | '_ \| |/ _ \  |  _ < / _ \| '_ \ / _` | |/ _` |______| |   | | | | __| __| |/ _` | | | |
# | |  | (_| | |_) | | (_) | | |_) | (_) | | | | (_| | | (_| |      | |___| |_| | |_| |_| | (_| | |_| |
# |_|   \__,_|_.__/|_|\___/  |____/ \___/|_| |_|\__,_|_|\__,_|      |______\__,_|\__|\__|_|\__,_|\__,_|
#



# This script is used to setup OpenVAS v21.04 on a Debian 11 fresh install


read -p "This script will install OpenVAS on a CLEAN DEBIAN 11. Proceed to installation ? (y/n)" -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi


# Colors
NC='\033[0m'
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'

# Run as root

if [ "$EUID" -ne 0 ]
  then echo -e "${RED}This script MUST be run as root${NC}"
  exit 1
fi


echo -e "${GREEN}Setting variables... Step 1/28 ${NC}"

# -----------------------------------------------
# VARIABLES

SCANNER_NAME="ESIEA"
ADMIN_USERNAME="admin"
ADMIN_PASSWORD="admin"
LOGFILE="setup_openvas.log"


# -----------------------------------------------


echo -e "${GREEN}Installing dependencies... ${NC}"
apt-get update
#apt install systemctl
curl -sL https://deb.nodesource.com/setup_16.x | bash -
apt-get install -y nodejs


# Uncomment this if you want ssh root access
#echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
#systemctl restart ssh sshd


echo -e "${GREEN}Creating user account... Step 2/28 ${NC}"

# Adding account for GVM
if ! id -u gvm &>/dev/null; then
  useradd -r -d /opt/gvm -c "GVM User" -s /bin/bash gvm
fi

mkdir /opt/gvm && chown gvm: /opt/gvm


echo -e "${GREEN}Installing build tools... Step 3/28 ${NC}"
# Install build tools
apt-get install -y gcc g++ make bison flex libksba-dev curl redis libpcap-dev cmake git pkg-config libglib2.0-dev libgpgme-dev nmap libgnutls28-dev uuid-dev libssh-gcrypt-dev libldap2-dev gnutls-bin libmicrohttpd-dev libhiredis-dev zlib1g-dev libxml2-dev libnet-dev libradcli-dev clang-format libldap2-dev doxygen gcc-mingw-w64 xml-twig-tools libical-dev perl-base heimdal-dev libpopt-dev libunistring-dev graphviz libsnmp-dev python3-setuptools python3-paramiko python3-lxml python3-defusedxml python3-dev gettext python3-polib xmltoman python3-pip texlive-fonts-recommended texlive-latex-extra xsltproc sudo vim rsync


echo -e "${GREEN}Installing Yarn... Step 4/28 ${NC}"
# Install Yarn
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/yarnkey.gpg >/dev/null
echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list
apt-get update
apt-get install -y yarn


echo -e "${GREEN}Installing PostgreSQL 11... Step 5/28 ${NC}"
# Install Postgresql 11
echo "deb http://apt.postgresql.org/pub/repos/apt bullseye-pgdg main" > /etc/apt/sources.list.d/pgdg.list
curl -sL https://www.postgresql.org/media/keys/ACCC4CF8.asc | tee /etc/apt/trusted.gpg.d/pgdg.asc >/dev/null
apt-get update
apt-get install -y postgresql-11 postgresql-contrib-11 postgresql-server-dev-11
service postgresql start

# Create Postgres user, database and roles

echo -e "${GREEN}Creating Postgres user, database and role... Step 6/28 ${NC}"
su postgres << 'ENDSU'
createuser gvm
createdb -O gvm gvmd
psql -U postgres gvmd <<EOF
CREATE ROLE dba WITH SUPERUSER NOINHERIT;
GRANT dba TO gvm;
EOF
ENDSU


echo -e "${GREEN}Restarting PostgreSQL and enabling it at boot... Step 7/28 ${NC}"
systemctl restart postgresql && systemctl enable postgresql


echo -e "${GREEN}Allow GVM user to sudo make install... Step 8/28 ${NC}"
# Allow GVM to execute make install as root
echo "gvm ALL = NOPASSWD: $(which make) install" > /etc/sudoers.d/gvmi


echo -e "${GREEN}Downloading OpenVAS source files from GitHub... Step 9/28 ${NC}"
# Download OpenVAS source files
su gvm << 'ENDSU'
cd

if [[ ! -d "gvm-source" ]]
then
  mkdir gvm-source
fi

cd gvm-source

pwd
git clone -b stable https://github.com/greenbone/gvm-libs.git
git clone -b main https://github.com/greenbone/openvas-smb.git
git clone -b stable https://github.com/greenbone/openvas.git
git clone -b stable https://github.com/greenbone/ospd.git
git clone -b stable https://github.com/greenbone/ospd-openvas.git
git clone -b stable https://github.com/greenbone/gvmd.git
git clone -b stable https://github.com/greenbone/gsad.git
git clone -b stable https://github.com/greenbone/gsa.git
ENDSU


echo -e "${GREEN}Building GVM libraries... Step 10/28 ${NC}"
su gvm << 'ENDSU'
# Build GVM libs
cd && cd gvm-source
pushd gvm-libs && mkdir build && cd build && cmake .. -DCMAKE_BUILD_TYPE=RELEASE && make -j4 && sudo make install && popd

ENDSU

echo -e "${GREEN}Building OpenVAS SMB module... Step 11/28 ${NC}"
su gvm << 'ENDSU'
# Build openvas-smb
cd && cd gvm-source
pushd openvas-smb && mkdir build && cd build && cmake .. -DCMAKE_BUILD_TYPE=RELEASE && make -j4 && sudo make install && popd

ENDSU

echo -e "${GREEN}Building OpenVAS Scanner... Step 12/28 ${NC}"
su gvm << 'ENDSU'
# Build openvas scanner
cd && cd gvm-source
pushd openvas && mkdir build && cd build && cmake .. -DCMAKE_BUILD_TYPE=RELEASE && make -j4 && sudo make install && popd

ENDSU

echo -e "${GREEN}Refreshing system libraries... Step 13/28 ${NC}"
# Load libraries
ldconfig


echo -e "${GREEN}Configuring Redis server... Step 14/28 ${NC}"
# Configure redis server
cp /opt/gvm/gvm-source/openvas/config/redis-openvas.conf /etc/redis/
chown redis:redis /etc/redis/redis-openvas.conf
echo "db_address = /run/redis-openvas/redis.sock" > /etc/openvas/openvas.conf
usermod -aG redis gvm
echo "net.core.somaxconn = 1024" >> /etc/sysctl.conf
echo 'vm.overcommit_memory = 1' >> /etc/sysctl.conf
sysctl -p

echo -e "${GREEN}Creating and enabling THP system service... Step 15/28 ${NC}"
# Configure THP service
cat > /etc/systemd/system/disable_thp.service << 'EOL'
[Unit]
Description=Disable Kernel Support for Transparent Huge Pages (THP)

[Service]
Type=simple
ExecStart=/bin/sh -c "echo 'never' > /sys/kernel/mm/transparent_hugepage/enabled && echo 'never' > /sys/kernel/mm/transparent_hugepage/defrag"

[Install]
WantedBy=multi-user.target
EOL

systemctl daemon-reload
systemctl enable --now disable_thp


echo -e "${GREEN}Enabling Redis server... Step 16/28 ${NC}"
# Start redis
systemctl enable --now redis-server@openvas


echo -e "${GREEN}Updating NVTs. This can take a long time, grab a coffee... Step 17/28 ${NC}"
# Update NVTs
chown -R gvm: /var/lib/openvas/
echo "gvm ALL = NOPASSWD: $(which openvas)" >> /etc/sudoers.d/gvm

su gvm <<'ENDSU'
cd
greenbone-nvt-sync
sudo openvas –-update-nvt-info
ENDSU


echo -e "${GREEN}Building GVMD... Step 18/28 ${NC}"
su gvm << 'ENDSU'
# Build gvmd
cd && cd gvm-source
pushd gvmd && mkdir build && cd build && cmake .. -DCMAKE_BUILD_TYPE=RELEASE && make -j4 && sudo make install && popd

ENDSU


echo -e "${GREEN}Building GSAD... Step 19/28 ${NC}"
su gvm << 'ENDSU'
# Build gsad
cd && cd gvm-source
pushd gsad && mkdir build && cd build && cmake .. -DCMAKE_BUILD_TYPE=RELEASE && make && sudo make install && popd
ENDSU

echo -e "${GREEN}Building GSA... Step 19.5/28 ${NC}"
mkdir -p /usr/local/share/gvm/gsad/web
chown -R gvm:gvm /usr/local/share/gvm
su gvm << 'ENDSU'
# Build gsa
cd && cd gvm-source
pushd gsa
rm -rf build
yarn
yarn build
cp -r build/* /usr/local/share/gvm/gsad/web
ENDSU


echo -e "${GREEN}Updating feeds. This can take a long time, grab a coffee... Step 20/28 ${NC}"
# Update the feeds
chown -R gvm: /var/lib/gvm/
su gvm << 'ENDSU'
greenbone-feed-sync --type GVMD_DATA
greenbone-feed-sync --type SCAP
greenbone-feed-sync --type CERT

# Generate certificates
gvm-manage-certs -a -f

ENDSU


echo -e "${GREEN}Building OSPD and OSPD-openvas... Step 21/28 ${NC}"
su gvm << 'ENDSU'
# Build OSPD and OSPD-openvas
cd
cd gvm-source
pip3 install wheel
pip3 install python-gvm gvm-tools
pushd ospd/ && python3 -m pip install . && popd && pushd ospd-openvas/ && python3 -m pip install . && popd

ENDSU


echo -e "${GREEN}Creating OpenVAS system unit... Step 22/28 ${NC}"
# Create OpenVAS system unit
cat > /etc/systemd/system/ospd-openvas.service << 'EOL'
[Unit]
Description=OSPd Wrapper for the OpenVAS Scanner (ospd-openvas)
After=network.target networking.service redis-server@openvas.service postgresql.service
Wants=redis-server@openvas.service
ConditionKernelCommandLine=!recovery

[Service]
ExecStartPre=-rm -rf /var/run/gvm/ospd-openvas.pid /var/run/gvm/ospd-openvas.sock
Type=simple
User=gvm
Group=gvm
RuntimeDirectory=gvm
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/gvm/bin:/opt/gvm/sbin:/opt/gvm/.local/bin
ExecStart=/opt/gvm/.local/bin/ospd-openvas \
--pid-file /var/run/gvm/ospd-openvas.pid \
--log-file /var/log/gvm/ospd-openvas.log \
--lock-file-dir /var/run/gvm -u /var/run/gvm/ospd-openvas.sock
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOL


echo -e "${GREEN}Enabling OSPD-openvas... Step 23/28 ${NC}"
# Setup GVM and OSPD
[[ -d /var/run/gvm ]] || mkdir /var/run/gvm
chown -R gvm: /var/run/gvm /var/log/gvm
systemctl daemon-reload && systemctl enable --now ospd-openvas



echo -e "${GREEN}Creating gvmd system unit and enabling it at boot... Step 24/28 ${NC}"
# Create gvmd system unit
cp /lib/systemd/system/gvmd.service{,.bak}
cat > /lib/systemd/system/gvmd.service << 'EOL'
[Unit]
Description=Greenbone Vulnerability Manager daemon (gvmd)
After=network.target networking.service postgresql.service ospd-openvas.service
Wants=postgresql.service ospd-openvas.service
Documentation=man:gvmd(8)
ConditionKernelCommandLine=!recovery

[Service]
Type=forking
User=gvm
Group=gvm
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/gvm/bin:/opt/gvm/sbin:/opt/gvm/.local/bin
ExecStart=/usr/local/sbin/gvmd --osp-vt-update=/var/run/gvm/ospd-openvas.sock
Restart=always
TimeoutStopSec=10

[Install]
WantedBy=multi-user.target
EOL
chown -R gvm:gvm /run/gvmd/
systemctl daemon-reload && systemctl enable --now gvmd


echo -e "${GREEN}Creating gsad system unit and enabling it at boot... Step 25/28 ${NC}"
# Create gsad system unit
cp /lib/systemd/system/gsad.service{,.bak}
cat > /lib/systemd/system/gsad.service << 'EOL'
[Unit]
Description=Greenbone Security Assistant daemon (gsad)
Documentation=man:gsad(8) https://www.greenbone.net
After=network.target gvmd.service
Wants=gvmd.service

[Service]
Type=simple
User=gvm
Group=gvm
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/gvm/bin:/opt/gvm/sbin:/opt/gvm/.local/bin
ExecStart=/usr/bin/sudo /usr/local/sbin/gsad -k /var/lib/gvm/private/CA/clientkey.pem -c /var/lib/gvm/CA/clientcert.pem
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOL

# Authorize gvm user to use gsad
echo "gvm ALL = NOPASSWD: $(which gsad)" >> /etc/sudoers.d/gvm

# Enable gsad
systemctl daemon-reload && systemctl enable --now gsad


echo -e "${GREEN}Creating and verifying scanner with name : '${SCANNER_NAME}'... Step 26/28 ${NC}"
# Create a scanner
sudo -u gvm gvmd --create-scanner=$SCANNER_NAME --scanner-type="OpenVAS" --scanner-host=/var/run/gvm/ospd-openvas.sock
SCANNER_UUID=$(sudo -u gvm gvmd --get-scanners | grep $SCANNER_NAME | cut -d' ' -f1)
sudo -u gvm gvmd --verify-scanner=$SCANNER_UUID
unset SCANNER_UUID


echo -e "${GREEN}Creating admin user... Login with the credentials in the UI : ${ADMIN_USERNAME}/${ADMIN_PASSWORD} Step 27/28${NC}"
# Create GVM admin user
sudo -u gvm gvmd --create-user $ADMIN_USERNAME --password=$ADMIN_PASSWORD
USER_UUID=$(sudo -u gvm gvmd --get-users --verbose | grep $ADMIN_USERNAME | cut -d' ' -f2)
echo $USER_UUID
sudo -u gvm gvmd --modify-setting 78eceaec-3385-11ea-b237-28d24461215b --value $USER_UUID
unset USER_UUID



echo -e "${GREEN}Unset variables and remove source files... Step 28/28 ${NC}"
# Cleanup variables
unset ADMIN_USERNAME
unset ADMIN_PASSWORD
unset SCANNER_NAME


# Cleanup files
su gvm <<'ENDSU'
cd
rm -rf gvm-source/
ENDSU

echo -e "${BLUE}All done ! Enjoy${NC}"
