#!/bin/bash

set -e

if [ "$EUID" -ne 0 ]
  then echo "Please run this script as root"
  exit
fi


source functions.sh

export MAKEFLAGS='-j4'

print_blue "Installing Prerequistes..."

apt-get update -qq > /dev/null
apt-get install make gcc g++ libssl-dev autoconf m4 wget patch git file libtool automake gettext lbzip2 -qq > /dev/null

mkdir -p sources

print_blue "Installing Software..."


/bin/bash jemalloc/install.sh
/bin/bash dig/install.sh
/bin/bash scripts_miroir_tcp/install.sh
/bin/bash golang/install.sh
/bin/bash gobuster/install.sh
/bin/bash nmap/install.sh
/bin/bash wpscan/install.sh
/bin/bash sqlmap/install.sh
/bin/bash netdiscover/install.sh
/bin/bash linpeas/install.sh
/bin/bash metasploit/install.sh
/bin/bash hydra/install.sh
/bin/bash tcpdump/install.sh
/bin/bash scapy/install.sh

print_blue "Uninstalling software..."

apt-get remove cups network-manager exim4 rpcbind avahi-daemon -qq


rm -rf .cache/ go/ .bash_history .wget-hsts

unset MAKEFLAGS
