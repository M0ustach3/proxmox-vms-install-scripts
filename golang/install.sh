#!/bin/bash
MAJORVERSION=1
PKGVERSION=1.18

source functions.sh

pushd sources/ > /dev/null

print_blue "Installing golang..."
wget -q "https://go.dev/dl/go${PKGVERSION}.linux-amd64.tar.gz" -O "go${PKGVERSION}.linux-amd64.tar.gz"
rm -rf /usr/local/go && tar -C /usr/local -xzf "go${PKGVERSION}.linux-amd64.tar.gz"
echo 'export PATH=$PATH:/usr/local/go/bin' | tee -a /etc/profile > /dev/null
source /etc/profile
rm -rf "go${PKGVERSION}.linux-amd64.tar.gz"
popd > /dev/null
