source functions.sh

print_blue "Installing scapy..."

MAJORVERSION=
PKGVERSION=


requirements(){
  print_green "\t[REQUIREMENTS] Getting requirements..."
  apt-get install python3 libjpeg-dev zlib1g-dev -qq > /dev/null
  apt-get install python3-pip -qq > /dev/null
}

download(){
  return 0
}

verify(){
  return 0
}

prepare(){
  return 0
}


build() {
  return 0
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  pip3 install -q --pre scapy[complete]
}

cleanup(){
  return 0
}

pushd sources/ > /dev/null

requirements
download
verify
prepare
build
install_package
cleanup

popd > /dev/null

unset PKGVERSION
unset MAJORVERSION
