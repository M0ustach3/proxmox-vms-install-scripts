source functions.sh

print_blue "Installing netdiscover..."

MAJORVERSION=0
PKGVERSION=0.9


requirements(){
  apt-get install libpcap-dev libnet1-dev dos2unix -qq > /dev/null
}

download(){
  print_green "\t[DOWNLOAD] Getting source code..."
  wget -q "https://github.com/netdiscover-scanner/netdiscover/archive/refs/tags/${PKGVERSION}.tar.gz" -O "netdiscover-${PKGVERSION}.tar.gz"
}

verify(){
  print_green "\t[VERIFY] Verifying package..."
  echo "dc436992cae0e61ad8d14e1fb4d2650ec9e754c0024010e15d341afb98e28f70 netdiscover-${PKGVERSION}.tar.gz" > "netdiscover-${PKGVERSION}.tar.gz.sha256sum"
  sha256sum -c "netdiscover-${PKGVERSION}.tar.gz.sha256sum" > /dev/null
}

prepare(){
  print_green "\t[PREPARE] Preparing build..."
  mkdir "netdiscover-${PKGVERSION}"
  tar -xzf "netdiscover-${PKGVERSION}.tar.gz" -C "netdiscover-${PKGVERSION}" --strip-components 1
  pushd "netdiscover-${PKGVERSION}" > /dev/null
  patch "update-oui-database.sh" "../../netdiscover/update-oui-database.patch" > /dev/null
  ./update-oui-database.sh --insecure > /dev/null
  ./autogen.sh
  ./configure > /dev/null
}


build() {
  print_green "\t[BUILD] Building package..."
  make "${MAKEFLAGS}" > /dev/null
  return 0
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  make install > /dev/null
  popd
}

cleanup(){
  print_green "\t[CLEANUP] Cleaning up..."
  rm -rf "netdiscover-${PKGVERSION}.tar.gz" "netdiscover-${PKGVERSION}" "netdiscover-${PKGVERSION}.tar.gz.sha256sum"
}

pushd sources/ > /dev/null

requirements
download
verify
prepare
build
install_package
cleanup

popd > /dev/null

unset PKGVERSION
unset MAJORVERSION
