#!/bin/bash

source functions.sh

print_blue "Installing TCP mirror scripts"

MAJORVERSION=
PKGVERSION=

requirements(){
  return 0
}

download(){
  return 0
}

verify(){
  return 0
}

prepare(){
  return 0
}


build() {
  return 0
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  install -D -m 700 -o root -g root scripts_miroir_tcp/*.sh /bin
}

cleanup(){
  return 0
}

requirements
download
verify
prepare
build
install_package
cleanup

unset PKGVERSION
unset MAJORVERSION
