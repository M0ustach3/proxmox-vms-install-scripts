#!/bin/bash
#Script de lancement du scan ICMP

PING=`which ping`
CAT=`which cat`
CUT=`which cut`
WHOAMI=`which whoami`

DEST_FILE="./protocol_icmp_sortie.txt"

#Vérification des droits
CURRENT_USER=`$WHOAMI`
if [[ $CURRENT_USER != "root" ]]
then
        echo "Vous devez avoir les droits root pour exécuter ce fichier"
        exit -1
fi

#Vérification du nombre d'arguments
if [ $# != 1 ]
then
	echo "Usage: $0 {miroir_ip_add}"
	exit -2
fi

#Vérification de la cohérence de l'argument
PTN="\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b"
RET=$(echo $1 | egrep -x $PTN)
if [[ "$?" -ne 0 ]]
then
	echo "Erreur adresse ip invalide"
	exit -3
fi

#Test du protocol ICMP en sortie à l'aide d'un ping
PTN="([0-9]?){2}[0-9]%"
RET=`$PING -c 1 -w 5 $1 | egrep -o $PTN | $CUT -d '%' -f1`
if [ $RET -ne 100 ]
then
	echo "Le protocol ICMP est autorisé en sortie" > $DEST_FILE
else
	echo "Le protocol ICMP est filtré en sortie" > $DEST_FILE
fi

#Affichage du résultat
echo "Ci-dessous le résultat du test en sortie du protocol ICMP:"
$CAT $DEST_FILE
exit 0
