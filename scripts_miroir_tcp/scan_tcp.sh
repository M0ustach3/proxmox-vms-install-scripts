#!/bin/bash
#Script de lancement du scan TCP

NMAP=`which nmap`
TEE=`which tee`
CAT=`which cat`
CUT=`which cut`
WHOAMI=`which whoami`

NMAP_FILE="./rapport_nmap_ports_tcp_ouverts_sortie.txt"
DEST_FILE="./ports_tcp_ouverts_sortie.txt"

#Vérification des droits
CURRENT_USER=`$WHOAMI`
if [[ $CURRENT_USER != "root" ]]
then
       echo "Vous devez avoir les droits root pour exécuter ce fichier"
       exit -1
fi

#Vérification de la présence du paquetage nmap
if [ -z $NMAP ]
then
	echo "Veuillez installer NMAP  pour utiliser ce script"
	exit -2
fi

#Vérification du nombre d'arguments
if [ $# != 2 ]
then
	echo "Usage: $0 {miroir_ip_add} {port_to_test|port_to_test_min-port_to_test_max}"
	exit -3
fi

#Vérification de la cohérence des arguments
PTN="\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b"
RET=$(echo $1 | egrep -x $PTN)
if [[ "$?" -ne 0 ]]
then
	echo "Erreur adresse ip invalide"
	exit -4
fi
PTN="\b((6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[0-5]?([0-9]?){3}[0-9])|((6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[0-5]?([0-9]?){3}[0-9])-(6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[0-5]?([0-9]?){3}[0-9])))\b"
RET=$(echo $2 | egrep -x $PTN)
if [[ "$?" -ne 0 ]]
then
	echo "Erreur port ou plage de ports invalide"
	exit -5
fi
PORT_MIN=`echo $2 | $CUT -d "-" -f1`
PORT_MAX=`echo $2 | $CUT -d "-" -f2`
if [[ $PORT_MAX < $PORT_MIN ]]
then
	echo "Erreur veuillez inverser les valeurs de la plage de ports"
	exit -6
fi

#Initialisation du fichier de sortie
echo "Récapitulatif des ports TCP ouverts en sortie dans l'intervalle $PORT_MIN-$PORT_MAX" > $DEST_FILE

#Lancement de nmap en mode furtif
$NMAP -sS -v -r -Pn -T Polite -p $PORT_MIN-$PORT_MAX $1 | $TEE /dev/tty > $NMAP_FILE

#Remplissage du fichier de sortie
PTN="\b(^[0-9]([0-9]?){4}.*open)\b"
OPEN_TCP_PORT=`$CAT $NMAP_FILE | egrep -o $PTN | $CUT -d '/' -f1`
rm -f $NMAP_FILE
for PORT in $OPEN_TCP_PORT
do
	echo "Le port TCP $PORT est ouvert en sortie" >> $DEST_FILE
done

#Récapitulatif des ports TCP ouverts
$CAT $DEST_FILE
exit 0
