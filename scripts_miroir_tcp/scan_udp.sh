#!/bin/bash
#Script de lancement du scan UDP

SEQ=`which seq`
NC=`which nc`
CAT=`which cat`
CUT=`which cut`
WHOAMI=`which whoami`

DEST_FILE="./ports_udp_ouverts_sortie.txt"

#Vérification des droits
CURRENT_USER=`$WHOAMI`
if [[ $CURRENT_USER != "root" ]]
then
	echo "Vous devez avoir les droits root pour exécuter ce fichier"
	exit -1
fi

#Vérification de la présence du paquetage netcat traditional
if [[ -z $NC ]]
then
	echo "Veuillez installer NETCAT (traditional)  pour utiliser ce script"
	exit -3
fi

#Vérification du nombre d'arguments
if [[ $# != 2 ]]
then
	echo "Usage: $0 {miroir_ip_add} {port_to_test|port_to_test_min-port_to_test_max}"
	exit -2
fi

#Vérification de la cohérence des arguments
PTN="\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b"
RET=$(echo $1 | egrep -x $PTN)
if [[ "$?" -ne 0 ]]
then
	echo "Erreur adresse ip invalide"
	exit -4
fi
PTN="\b((6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[0-5]?([0-9]?){3}[0-9])|((6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[0-5]?([0-9]?){3}[0-9])-(6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[0-5]?([0-9]?){3}[0-9])))\b"
RET=$(echo $2 | egrep -x $PTN)
if [[ "$?" -ne 0 ]]
then
	echo "Erreur port ou plage de ports invalide"
	exit -5
fi
PORT_MIN=`echo $2 | $CUT -d "-" -f1`
PORT_MAX=`echo $2 | $CUT -d "-" -f2`
if [[ $PORT_MAX < $PORT_MIN ]]
then
	echo "Erreur veuillez inverser les valeurs de la plage de ports"
	exit -6
fi

#Initialisation du fichier de sortie
echo "Récapitulatif des ports UDP ouverts en sortie dans l'intervalle $PORT_MIN-$PORT_MAX" > $DEST_FILE


#Pour chaque port, on test s'il est ouvert
for PORT in `$SEQ $PORT_MIN $PORT_MAX`
do
	IP=$1
	MIROR_ANSWER=`echo "echo" | "$NC" -u -w 1 "$IP" "$PORT"`
	if [[ ! -z $MIROR_ANSWER ]]
	then
		echo "Le port UDP $PORT est ouvert en sortie"
		echo "Le port UDP $PORT est ouvert en sortie" >> $DEST_FILE
	else
		echo "Le port UDP $PORT est fermé en sortie"
	fi
done

#Récapitulatif des ports UDP ouverts
$CAT $DEST_FILE
exit 0
