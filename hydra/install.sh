source functions.sh

print_blue "Installing hydra..."

MAJORVERSION=9
PKGVERSION=9.3


requirements(){
  return 0
}

download(){
  print_green "\t[DOWNLOAD] Getting source code..."
  wget -q "https://github.com/vanhauser-thc/thc-hydra/archive/refs/tags/v${PKGVERSION}.tar.gz" -O "hydra-${PKGVERSION}.tar.gz"
}

verify(){
  print_green "\t[VERIFY] Verifying package..."
  echo "3977221a7eb176cd100298c6d47939999a920a628868ae1aceed408a21e04013 hydra-${PKGVERSION}.tar.gz" > "hydra-${PKGVERSION}.tar.gz.sha256sum"
  sha256sum -c "hydra-${PKGVERSION}.tar.gz.sha256sum" > /dev/null
}

prepare(){
  print_green "\t[PREPARE] Preparing build..."
  mkdir "hydra-${PKGVERSION}"
  tar -xzf "hydra-${PKGVERSION}.tar.gz" -C "hydra-${PKGVERSION}" --strip-components 1
  pushd "hydra-${PKGVERSION}" > /dev/null
  ./configure > /dev/null
}


build() {
  print_green "\t[BUILD] Building package..."
  make "${MAKEFLAGS}" > /dev/null
  return 0
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  make install > /dev/null
  popd
}

cleanup(){
  print_green "\t[CLEANUP] Cleaning up..."
  rm -rf "hydra-${PKGVERSION}.tar.gz" "hydra-${PKGVERSION}" "hydra-${PKGVERSION}.tar.gz.sha256sum"
}

pushd sources/ > /dev/null

requirements
download
verify
prepare
build
install_package
cleanup

popd > /dev/null

unset PKGVERSION
unset MAJORVERSION
