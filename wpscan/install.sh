#!/bin/bash

source functions.sh

print_blue "Installing WPScan"

MAJORVERSION=
PKGVERSION=

requirements(){
  apt-get install ruby build-essential libcurl4-openssl-dev libxml2 libxml2-dev libxslt1-dev ruby-dev libgmp-dev zlib1g-dev -qq > /dev/null
}

download(){
  return 0
}

verify(){
  return 0
}

prepare(){
  return 0
}


build() {
  return 0
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  gem install wpscan --quiet --silent
}

cleanup(){
  return 0
}

requirements
download
verify
prepare
build
install_package
cleanup

unset PKGVERSION
unset MAJORVERSION
