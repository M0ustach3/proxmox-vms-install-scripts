# Proxmox-vms-install-scripts
Ce projet permet de gagner du temps en automatisant l'installation des différents outils d'audit sur les machines Proxmox. Il a été réalisé initialement par Pablo BONDIA-LUTTIAU et Théophile HAMELIN, promo 2022.

Pour ajouter un nouvel outil, créer un nouveau dossier et reprendre le code d'un des fichiers ```install.sh``` existants. Remplacer et adapter le contenu en fonction.

Appelez ensuite le script nouvellement créé dans le fichier ```install_tools.sh```.

Un script d'installation d'OpenVAS sur une Debian 11 est également disponible à la racine du projet. ATTENTION : Ce script à été réalisé en début d'année 2022, et peut donc ne pas fonctionner avec de nouvelles versions d'OpenVAS et de Debian. Use at your own risk.
