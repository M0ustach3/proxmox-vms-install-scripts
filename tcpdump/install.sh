#!/bin/bash

source functions.sh


install_libpcap(){
  _MAJORVERSION=1
  _PKGVERSION=1.10.1

  apt-get install flex bison -qq > /dev/null

  wget -q "https://www.tcpdump.org/release/libpcap-${_PKGVERSION}.tar.gz" -O "libpcap-${_PKGVERSION}.tar.gz"
  echo "ed285f4accaf05344f90975757b3dbfe772ba41d1c401c2648b7fa45b711bdd4 libpcap-${_PKGVERSION}.tar.gz" > "libpcap-${_PKGVERSION}.tar.gz.sha256sum"
  sha256sum -c "libpcap-${_PKGVERSION}.tar.gz.sha256sum" > /dev/null
  mkdir -p "libpcap-${_PKGVERSION}"
  tar -xf "libpcap-${_PKGVERSION}.tar.gz" -C "libpcap-${_PKGVERSION}" --strip-components 1
  pushd "libpcap-${_PKGVERSION}" > /dev/null
  ./configure > /dev/null
  make "${MAKEFLAGS}" > /dev/null
  make install > /dev/null
  popd > /dev/null
  rm -rf "libpcap-${_PKGVERSION}.tar.gz.sha256sum" "libpcap-${_PKGVERSION}" "libpcap-${_PKGVERSION}.tar.gz"
  unset _MAJORVERSION
  unset _PKGVERSION
}

print_blue "Installing TCPdump..."

MAJORVERSION=4
PKGVERSION=4.99.1

requirements(){
  print_green "\t[REQUIREMENTS] Installing libpcap..."
  install_libpcap
}

download(){
  print_green "\t[DOWNLOAD] Getting source code..."
  wget -q "https://www.tcpdump.org/release/tcpdump-${PKGVERSION}.tar.gz" -O "tcpdump-${PKGVERSION}.tar.gz"
}

verify(){
  print_green "\t[VERIFY] Verifying package..."
  echo "79b36985fb2703146618d87c4acde3e068b91c553fb93f021a337f175fd10ebe tcpdump-${PKGVERSION}.tar.gz" > "tcpdump-${PKGVERSION}.tar.gz.sha256sum"
  sha256sum -c "tcpdump-${PKGVERSION}.tar.gz.sha256sum" > /dev/null
}

prepare(){
  print_green "\t[PREPARE] Preparing build..."
  mkdir -p "tcpdump-${PKGVERSION}"
  tar -xf "tcpdump-${PKGVERSION}.tar.gz" -C "tcpdump-${PKGVERSION}" --strip-components 1
  pushd "tcpdump-${PKGVERSION}" > /dev/null
  ./configure > /dev/null
}


build() {
  print_green "\t[BUILD] Building package..."
  make "${MAKEFLAGS}" > /dev/null
  return 0
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  make install > /dev/null
  popd > /dev/null
}

cleanup(){
  print_green "\t[CLEANUP] Cleaning up..."
  rm -rf "tcpdump-${PKGVERSION}.tar.gz" "tcpdump-${PKGVERSION}" "tcpdump-${PKGVERSION}.tar.gz.sha256sum"
}

pushd sources/ > /dev/null

requirements
download
verify
prepare
build
install_package
cleanup

popd > /dev/null

unset PKGVERSION
unset MAJORVERSION
