#!/bin/bash

source functions.sh

print_blue "Installing gobuster..."

MAJORVERSION=3
PKGVERSION=3.1.0

requirements(){
  print_green "\t[REQUIREMENTS] Getting requirements..."
  source /etc/profile
  return 0
}

download(){
  print_green "\t[DOWNLOAD] Getting source code..."
  wget -q "https://github.com/OJ/gobuster/archive/refs/tags/v${PKGVERSION}.tar.gz" -O "gobuster-linux-amd64-${PKGVERSION}.tar.gz"
}

verify(){
  print_green "\t[VERIFY] Verifying package..."
  echo "a49e597412a0be68020f2836c4f33276cb653d00543f59d4cff34b053b8d9a10 gobuster-linux-amd64-${PKGVERSION}.tar.gz" > "gobuster-linux-amd64-${PKGVERSION}.tar.gz.sha256sum"
  sha256sum -c "gobuster-linux-amd64-${PKGVERSION}.tar.gz.sha256sum" > /dev/null
}

prepare(){
  print_green "\t[PREPARE] Preparing build..."
  mkdir "gobuster-linux-amd64-${PKGVERSION}"
  tar -xf "gobuster-linux-amd64-${PKGVERSION}.tar.gz" -C "gobuster-linux-amd64-${PKGVERSION}" --strip-components 1
  pushd "gobuster-linux-amd64-${PKGVERSION}" > /dev/null
  patch -Np1 "libgobuster/helpers.go" "../../gobuster/patch_gobuster.patch" > /dev/null
  go get > /dev/null 2>&1
}


build() {
  print_green "\t[BUILD] Building package..."
  go build > /dev/null
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  install -D -m 700 -o root -g root gobuster /bin
  popd > /dev/null
}

cleanup(){
  print_green "\t[CLEANUP] Cleaning up..."
  rm -rf "gobuster-linux-amd64-${PKGVERSION}" "gobuster-linux-amd64-${PKGVERSION}.tar.gz" "gobuster-linux-amd64-${PKGVERSION}.tar.gz.sha256sum"
}

pushd sources/ > /dev/null

requirements
download
verify
prepare
build
install_package
cleanup

popd > /dev/null

unset PKGVERSION
unset MAJORVERSION
