source functions.sh

print_blue "Installing linPEAS..."

MAJORVERSION=
PKGVERSION=


requirements(){
  apt-get install curl -qq > /dev/null
}

download(){
  print_green "\t[DOWNLOAD] Getting source code..."
  curl -L https://github.com/carlospolop/PEASS-ng/releases/latest/download/linpeas.sh > linpeas.sh
}

verify(){
  return 0
}

prepare(){
  return 0
}


build() {
  return 0
}


install_package(){
  print_green "\t[INSTALL] Installing package..."
  install -D -m 700 -o root -g root linpeas.sh /bin
}

cleanup(){
  rm -rf linpeas.sh
  return 0
}

pushd sources/ > /dev/null

requirements
download
verify
prepare
build
install_package
cleanup

popd > /dev/null

unset PKGVERSION
unset MAJORVERSION
